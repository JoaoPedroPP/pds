function Convolucao(x, h)
%  disp("Colunas de x(t):");
  [lx cx] = size(x');
%  disp("Colunas de h(t)");
  [lh ch] = size(h);
  
  l = 2*ch+1-mod(ch,2);
  c = lx;
  d = ch
  
  H = zeros(l, c);
  for i = 1:c
    count = 1;
    for j = i:l
      if count <= ch
        H(j, i) = h(count);
      endif
      count++;
    endfor
  endfor
  Y = H*x';% Convolucao implementada
  YC = conv(h,x);% Convolucao via Conv
  figure(1)
  stem(Y);% Convolucao implementada
  figure(2)
  stem(YC);% Convolucao via Conv
endfunction