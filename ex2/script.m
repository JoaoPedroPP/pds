close all;
clear all;
clc;

%Definicao de alguns valores de teste
Fa = 44100; %Frequencia de amostragem
t = (0:1000)/Fa; %Vetor de tempo, escalonado em cima da frequencia de amostragem
f = 20000; %Frequencia de um pulso qualquer
y = sin(2*pi*f*t); %Sinal de audio qualquer

%Obtencao do tamanho do sinal, e a quantidade de colunas que nos interessa
[ax ay] = size(y);

%Amostramento do sinal
N = ay;
Xa  = zeros(ax, ay);

for k = 1:N-1
  for n = 1:N-1
    Xa(k) = Xa(k) + y(n)*exp(-j*2*pi*k*n/N);
  endfor
endfor

fRad = 0:2*pi/(length(t)-1):2*pi;
fHz = 0:Fa/(length(t)-1):Fa;


figure(1)
subplot(211), plot(fRad, Xa, 'LineWidth', 2), xlabel('\pi rad amostras'), ylabel('Amplitude'), grid on, axis([0 length(fRad) -30 100])
subplot(212), plot(fHz, Xa, 'LineWidth', 2), xlabel('Hz'), ylabel('Amplitude'), grid on, axis([0 length(fHz) -30 100])

%Vamos reamostrar o sinal a uma Fa menor

Fa2 = 4410;
t2 = (1:1000):Fa2;
y2 = sin(2*pi*f*t2); %Sinal de audio qualquer
[ax2 ay2] = size(y2);
Xa2  = zeros(ax2, ay2);

for k = 1:N-1
  for n = 1:N-1
    Xa2(k) = Xa2(k) + y2(n)*exp(-j*2*pi*k*n/N);
  endfor
endfor

fRad2 = 0:2*pi/(length(t2)-1):2*pi;
fHz2 = 0:Fa2/(length(t2)-1):Fa2;

figure(2)
subplot(211), plot(fRad2, Xa2, 'LineWidth', 2), xlabel('\pi rad amostras'), ylabel('Amplitude'), grid on, axis([0 length(fRad2) -30 100])
subplot(212), plot(fHz2, Xa2, 'LineWidth', 2), xlabel('Hz'), ylabel('Amplitude'), grid on, axis([0 length(fHz2) -30 100])
