close all;
clear all;
clc;
pkg load signal;

% Musica
[sig_column_stereo, Fs] = audioread('song_20s.wav');
sig_column_mono = sum(sig_column_stereo, 2) / size(sig_column_stereo, 2);
sig = sig_column_mono';

% Tempo de dura��o da m�sica
d = length(sig)/Fs;

% Vetor tempo para plot do sinal original
t_sig = [1/Fs:1/Fs:d];

% Toca o sinal original
sound(sig', Fs);

% Plot sinal original
figure(1);
subplot(3,1,1);
plot(t_sig, sig);
title('Signal - Original Sample');

% Calcula a DFT do sinal original
y = fft(sig);
k = 0:Fs/(length(y)-1):Fs;
m = abs(y);
p = unwrap(angle(y));

% Plot de magnitude e fase da DFT do sinal original
subplot(3,1,2);
plot(k, m);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude');
subplot(3,1,3);
plot(k, p);
xlabel('k');
ylabel('angle(X[k])');
title('Phase');




% Amostragem 44100Hz

% Novo sinal amostrado a 44100Hz
Fs_a = 44100;
sig_a = resample(sig,Fs_a,Fs);

% Vetor tempo para plot do sinal amostrado a 44100Hz
d_a = length(sig_a)/Fs_a;
t_a = [1/Fs_a:1/Fs_a:d_a];

% Toca o sinal amostrado a 44100Hz
sound(sig_a', Fs_a);

% Plot sinal amostrado a 44100Hz
figure(2);
subplot(3,1,1);
plot(t_a, sig_a);
title('Signal - 44100Hz');

% Calcula a DFT do sinal amostrado a 44100Hz
y_a = fft(sig_a);
k_a = 0:Fs_a/(length(y_a)-1):Fs_a;
m_a = abs(y_a);
p_a = unwrap(angle(y_a));

% Plot de magnitude e fase da DFT do sinal amostrado a 44100Hz
subplot(3,1,2);
plot(k_a, m_a);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude - 44100Hz');
subplot(3,1,3);
plot(k_a, p_a);
xlabel('k');
ylabel('angle(X[k])');
title('Phase - 44100Hz');





% Amostragem 4410Hz

% Novo sinal amostrado
Fs_b = 4410;
sig_b = resample(sig,Fs_b,Fs);

% Vetor tempo para plot do sinal amostrado a 4410Hz
d_b = length(sig_b)/Fs_b;
t_b = [1/Fs_b:1/Fs_b:d_b];

% Toca o sinal amostrado a 4410Hz
sound(sig_b', Fs_b);

% Plot sinal amostrado a 4410Hz
figure(3);
subplot(3,1,1);
plot(t_b, sig_b);
title('Signal - 4410Hz');

% Calcula a DFT do sinal amostrado a 4410Hz
y_b = fft(sig_b);
k_b = 0:Fs_b/(length(y_b)-1):Fs_b;
m_b = abs(y_b);
p_b = unwrap(angle(y_b));

% Plot de magnitude e fase da DFT do sinal amostrado a 4410Hz
subplot(3,1,2);
plot(k_b, m_b);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude - 4410Hz');
subplot(3,1,3);
plot(k_b, p_b);
xlabel('k');
ylabel('angle(X[k])');
title('Phase - 4410Hz');
