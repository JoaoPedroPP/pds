function Xk=dft_fun(xn, N)
  L = length(xn);
  if (N<L)
    disp(L);
    error('N should be greater than or equal L');
  endif
  
  xn=[xn zeros(1,N-L)];
  for k=0:N-1
    for n=0:N-1
      Wn=exp(-j*2*pi*k*n/N);
      X1(k+1,n+1)=Wn;
    endfor
  endfor
  Xk=X1*xn';
endfunction
