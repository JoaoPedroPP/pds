close all;
clear all;
clc;
pkg load signal;

% Musica
[sig_column_stereo, Fs] = audioread('song_20s.wav');
sig_column_mono = sum(sig_column_stereo, 2) / size(sig_column_stereo, 2);
sig = sig_column_mono';

% Tempo de dura��o da m�sica
d = length(sig)/Fs;

% Vetor tempo para plot do sinal original
t_sig = [1/Fs:1/Fs:d];

% Plot sinal original
figure(1);
subplot(3,1,1);
plot(t_sig, sig);
title('Signal - Original Sample');

% Calcula a DFT do sinal original
y = fft(sig);
k = 0:Fs/(length(y)-1):Fs;
m = abs(y);
p = unwrap(angle(y));

% Plot de magnitude e fase da DFT do sinal original
subplot(3,1,2);
plot(k, m);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude');
subplot(3,1,3);
plot(k, p);
xlabel('k');
ylabel('angle(X[k])');
title('Phase');

% Filtro de m�dia m�vel N = 3
N = 25;
h = 1/N*ones(N,1);

% Filtrar o sinal original
sig_f = filter(h,1,sig);

% Toca o sinal filtrado
sound(sig_f', Fs);

% Plot sinal filtrado
figure(2);
subplot(3,1,1);
plot(t_sig, sig_f);
title('Signal - Filtered Sample');

% Calcula a DFT do sinal filtrado
y_f = fft(sig_f);
m_f = abs(y_f);
p_f = unwrap(angle(y_f));

% Plot de magnitude e fase da DFT do sinal filtrado
subplot(3,1,2);
plot(k, m_f);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude');
subplot(3,1,3);
plot(k, p_f);
xlabel('k');
ylabel('angle(X[k])');
title('Phase');


% Amostragem 4410Hz

% Novo sinal amostrado a partir do sinal filtrado
Fs_b = 4410;
sig_b = resample(sig_f,Fs_b,Fs);

% Vetor tempo para plot do sinal amostrado a 4410Hz
d_b = length(sig_b)/Fs_b;
t_b = [1/Fs_b:1/Fs_b:d_b];

% Toca o sinal amostrado a 4410Hz
sound(sig_b', Fs_b);

% Plot sinal amostrado a 4410Hz
figure(3);
subplot(3,1,1);
plot(t_b, sig_b);
title('Signal - 4410Hz');

% Calcula a DFT do sinal amostrado a 4410Hz
y_b = fft(sig_b);
k_b = 0:Fs_b/(length(y_b)-1):Fs_b;
m_b = abs(y_b);
p_b = unwrap(angle(y_b));

% Plot de magnitude e fase da DFT do sinal amostrado a 4410Hz
subplot(3,1,2);
plot(k_b, m_b);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude - 4410Hz');
subplot(3,1,3);
plot(k_b, p_b);
xlabel('k');
ylabel('angle(X[k])');
title('Phase - 4410Hz');


% Plot das magnitudes de todos sinais para compara��o
figure(4);
subplot(3,1,1);
plot(k, m);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude - Original Sample');
subplot(3,1,2);
plot(k, m_f);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude - Filtered Sample');
subplot(3,1,3);
plot(k_b, m_b);
xlabel('k');
ylabel('|X[k]|');
title('Magnitude - 4410Hz');
