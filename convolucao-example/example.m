% Exemplo convolu��o
% Convolui um sinal de �udio com o tiro de uma arma. O tiro da arma serve
% como resposta ao impulso do sistema, uma vez que se trata de um sinal
% extremamente curto no tempo. Com isso, serve para extrair a resposta do
% impulso do ambiente em que o tiro foi realizado. Portanto, convoluir o
% som do tiro com um sinal de um instrumento, resulta em uma previs�o do
% que seria esse instrumento ser tocado no ambiente do tiro.
%
% Autor: Pedro Ivo da Cruz
% �ltima modifica��o: 15/06/2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear variables; close all;

% Carregando os �udios de m�sica e tiros:
[sig, Fs] = audioread('sample.wav');
%[shot, Fs] = audioread('shot.wav');
%[shot2, Fs] = audioread('shot2.mp3');

% Algumas vers�es do Matlab mais antigas n�o possuem a fun��o audioread e,
% portanto, � necess�rio utilzar a fun��o wavread abaixo. No entanto, esta
% fun��o n�o � capaz de ler arquivos mp3, portanto comente as etapas do
% c�digo que utilizam o 'shot2'.
%[sig, Fs] = wavread('sample.wav');
%[shot, Fs] = wavread('shot.wav');

% Convertendo os sinais de tiro para mono:
shot = 0.5*(shot(:, 1) + shot(:, 2));
shot2 = 0.5*(shot2(:, 1) + shot2(:, 2));

% Executa o programa
fprintf('Aperte Enter para tocar o som do primeiro tiro \n')
pause;
sound(shot, Fs) % Toca o primeiro tiro

fprintf('Aperte Enter para tocar o som da m�sica \n')
pause;
sound(sig, Fs) % Toca o sinal original

y = conv(sig, shot); % Convolui o sinal com o tiro
y = y/max(y); % Normaliza o resultado da convolu��o
fprintf('Aperte Enter para tocar o som da m�sica no ambiente do tiro \n')
pause;
sound(y, Fs) % Toca o sinal convolu�do com o tiro

fprintf('Aperte Enter para tocar o som do segundo tiro \n')
pause;
sound(shot, Fs) % Toca o segundo tiro

y = conv(sig, shot2); % Convolui o sinal com o tiro
y = y/max(y); % Normaliza o resultado da convolu��o
fprintf('Aperte Enter para tocar o som da m�sica no ambiente do tiro \n')
pause;
sound(y, Fs) % Toca o sinal convolu�do com o tiro